import time
import cloudscraper
import requests
import selenium.webdriver.remote.webdriver
from bs4 import BeautifulSoup
import os
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

import string
letters = list(string.ascii_letters)
API_URL = "https://www.goodrx.com/api/v4/drug_concepts"
CWD = os.getcwd()
RESULTS_PATH = os.path.join(CWD, 'files', 'results.json')
DRUG_NAMES_BACKUP_PATH = os.path.join(CWD, 'files', 'all_drug_names_backup.json')
MOST_COMMON_MEDICIANS_PATH = os.path.join(CWD, 'files', 'most_common_medications.json')
ALL_DRUGS_NAMES_BY_LETTES_PATH = os.path.join(CWD, 'files', 'from_k_all_drug_names_by_letters.json')
FAILED_DUE_TO_VERIFICATION_PATH = os.path.join(CWD, 'files', 'failed_due_to_verification.json')
FAILED_DUE_TO_DRUG_NOT_FOUND_PATH = os.path.join(CWD, 'files', 'failed_due_to_drug_not_found.json')


driver = webdriver.Firefox()


def get_top_drugs(all_letters):
    headers = {"Host": "www.goodrx.com",
               "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0",
               "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
               "Accept-Language": "en-US,en;q=0.5", "Accept-Encoding": "gzip, deflate, br", "Connection": "keep-alive",
               "Upgrade-Insecure-Requests": "1", "Sec-Fetch-Dest": "document", "Sec-Fetch-Mode": "navigate",
               "Sec-Fetch-Site": "none", "Sec-Fetch-User": "?1",
               "If-None-Match": "W/\"49cdd-xbHFEg7qAMEtWjLAaYb+hgmYeG4\"", "Cache-Control": "max-age=0"}
    top_drugs_list = []
    for letter in all_letters:
        # driver = webdriver.Firefox()
        print(letter, end='\r')
        url = f'https://www.goodrx.com/drugs/{letter}'
        driver.get(url)
        response = driver.page_source
        soup = BeautifulSoup(response, "html.parser")
        top_drugs_list += [item.text for item in
                           soup.find("div", attrs={"class": "topDrugGrid-3ZxaH"}).find_all("span")]
        # driver.close()
    with open(MOST_COMMON_MEDICIANS_PATH, "w") as f:
        json.dump(top_drugs_list, f)
    return top_drugs_list


def get_all_drugs_names(all_letters):
    url = API_URL
    drugs_by_letters = {}
    for letter in all_letters[10:]:
        print(letter)
        page_token = 100
        concepts = []
        j = 1
        while len(concepts) == 0:
            # payload = {"page_size": 100, "page_token": page_token, "search": letter}
            time.sleep(2)
            req_url = f"{url}?page_size=100&page_token={page_token}&search={letter}"
            # driver = webdriver.Firefox()
            # driver.get(req_url)
            scraper = cloudscraper.create_scraper()
            r = scraper.get(req_url)
            # print(r.status_code)
            try:
                concepts = json.loads(r.content)['concepts']
            except:
                pass
            scraper.close()
            print(j, len(concepts), r.status_code)
            j += 1
        drugs_by_letters.update({letter: concepts})
        with open(ALL_DRUGS_NAMES_BY_LETTES_PATH, "w") as f:
            json.dump(drugs_by_letters, f)
    drugs_list = []
    for letter in drugs_by_letters:
        drugs_list += drugs_by_letters[letter]
    return drugs_list


def get_page_soup(page_url):
    driver.get(page_url)
    recapcha_page = None
    try:
        recapcha_page = driver.find_element_by_id("px-captcha")
        element = driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[0]).find_element_by_tag_name('body')
        ActionChains(driver).click_and_hold(element).perform()
        element = driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[1]).find_element_by_tag_name('body')
        ActionChains(driver).click_and_hold(element).perform()
        #click_and_hold(driver, recapcha_page)
        time.sleep(10)
        return 12345
    except Exception as e:
        return BeautifulSoup(driver.page_source, "html.parser")
        print(e)


def get_drug_details(soup, url):
    not_found = soup.find("div", attrs={"class": "row justify-content-md-center"})
    try:
        temp = not_found.find("h2").text
    except:
        temp = None
    if not_found and temp == "Drug Not Found":
        print(not_found.find("h2").text)
        return {}, soup
    else:
        price_div = soup.find("div", attrs={"id": "priceBox"})
        regular_price = price_div.find("span", attrs={"class": "fs-21 fs-sm-28"}).text.replace("\n", "").strip()
        main_save_percentage = soup.find("strong").text.replace("Save up to ", "")
        main_price = soup.find("h2").text.split("\n")[2].strip().replace(" with GoodRx\xa0Gold", "").replace(
            "Pay as little as ", "")
        main_description = soup.find_all("div", {"class": "row text-center"})[0].find("b").text
        average_retail_price = soup.find(id="cashPrice").text
        pharmacies_divs = price_div.find_all("div", attrs={"class": "participating-pharmacies fs-18"})
        drug_form = [option.text for option in soup.find(id="drugForms").find_all("option")]
        drug_dosage = [option.text for option in soup.find(id="drugDosages").find_all("option")]
        drug_quantities = [option.text for option in soup.find(id="drugQuantities").find_all("option")]
        price = regular_price
        phar_list = [item.text.strip() for item in pharmacies_divs[0].find_all("li")]
        pharmacies = '\n'.join(phar_list)
        save = price_div.find("p", attrs={"class": "text-green fs-16 text-right"})
        if save:
            save_value = save.text.replace("You save ", "").replace("\n", "")
        else:
            save_value = None
        price_list = [
            {"price": price,
             "pharmacies": pharmacies,
             "save_percentage": save_value}
        ]

        for item in pharmacies_divs[1:]:
            phar_list = [phar.text.strip() for phar in item.find_all("li")]
            pharmacies = '\n'.join(phar_list)
            price = item.find("span", attrs={"class": "fs-21 fs-sm-28"}).text.replace("\n", "").strip()
            save = item.find("p", attrs={"class": "text-green fs-16 text-right"})
            if save:
                save_value = save.text.replace("You save ", "").replace("\n", "")
            else:
                save_value = None
            price_list.append(
                {"price": price,
                 "pharmacies": pharmacies,
                 "save_percentage": save_value}
            )

        return {"main_save_percentage": main_save_percentage,
                "main_price": main_price,
                "main_description": main_description,
                "regular_price": regular_price,
                "average_retail_price": average_retail_price,
                "url": url,
                "drug_form": drug_form,
                "drug_dosage": drug_dosage,
                "drug_quantities": drug_quantities,
                "price_list": price_list}, soup


def get_new_drug_details(drug_url):
    page_soup = get_page_soup(drug_url)
    if page_soup != 12345:
        return get_drug_details(page_soup, drug_url)
    else:
        return 12345, 12345


def slugify(name):
    name_slug = name.replace(" (generic)", "").replace("(brand)", "").replace(" / ", "-").replace("/", "-").replace(
        " (", "-").replace(") ", "-").replace(")", "-").replace("(", "-").replace(" ", "-").lower()
    while name_slug[-1] == "-" or name_slug[-1] == "-":
        name_slug = name_slug[:-1]
    return name_slug


if __name__ == "__main__":
    with open(DRUG_NAMES_BACKUP_PATH, "r") as f:
        list_of_drugs = json.load(f)
        
    failed_due_to_drug_not_found = []
    failed_due_to_verification = []
    all_variants_details = {}
    s = time.time()
    for i, drug in enumerate(list_of_drugs):
        print(f"{i+1}/{len(list_of_drugs)}")
        slug = drug['slug']
        url = f'https://gold.goodrx.com/{slug}/price'
        print(url)
        try:
            variant_price_dict, soup = get_new_drug_details(url)
        except:
            failed_due_to_drug_not_found.append(drug)
            with open(FAILED_DUE_TO_DRUG_NOT_FOUND_PATH, "w") as f:
                json.dump(failed_due_to_drug_not_found, f)
            continue
        if variant_price_dict != 12345:
            if len(variant_price_dict) > 0:
                variant_price_dict.update({"id": i})
                all_variants_details.update({drug['name']: variant_price_dict})
                drug_labels = [option.text for option in soup.find(id="drugLabel").find_all("option")]
                if len(drug_labels) > 1:
                    for variant in drug_labels:
                        variant_slug = slugify(variant)
                        if variant_slug != slug:
                            url = f'https://gold.goodrx.com/{slug}/price?label={variant_slug}'
                            print(url)
                            variant_price_dict = get_new_drug_details(url)[0]
                            if variant_price_dict != 12345:
                                variant_price_dict.update({"id": i})
                                all_variants_details.update({variant: variant_price_dict})
                            else:
                                failed_due_to_verification.append(drug)
                                with open(FAILED_DUE_TO_VERIFICATION_PATH, "w") as f:
                                    json.dump(failed_due_to_verification, f)
                                driver.close()
                                # time.sleep(10)
                                driver = webdriver.Firefox()
                with open(RESULTS_PATH, "w") as f:
                    json.dump(all_variants_details, f)
        else:
            failed_due_to_verification.append(drug)
            with open(FAILED_DUE_TO_VERIFICATION_PATH, "w") as f:
                json.dump(failed_due_to_verification, f)
            driver.close()
            # time.sleep(5)
            driver = webdriver.Firefox()
        i += 1
        print(f"Total elapsed time: {(time.time() - s) / 60} minutes")
